from django.urls import path
from myapp.views import *

urlpatterns = [
    path('country', country_list.as_view(), name='country'),
    path('country/<str:id>/edit',country_details.as_view(),name='country_details'),
    path('customer', customer_list.as_view(), name='customer'),
    path('customer/<str:id>/edit',customer_details.as_view(),name='customer_details'),
    path('product_category', product_category_list.as_view(), name='product_category'),
    path('product_category/<str:id>/edit',product_category_details.as_view(),name='product_category_details'),
    path('product', product_list.as_view(), name='product'),
    path('product/<str:id>/edit',product_details.as_view(),name='product_details'),
    path('installation', installation_list.as_view(), name='installation'),
    path('installation/<str:id>/edit',installation_details.as_view(),name='installation_details'),
]