from django.contrib import admin
from myapp.models import *
# Register your models here.
admin.site.register(country)
admin.site.register(customer)
admin.site.register(product_category)
admin.site.register(product)
admin.site.register(installation)

