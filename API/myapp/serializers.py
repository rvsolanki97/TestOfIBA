from myapp.models import *
from rest_framework import serializers

class Country(serializers.ModelSerializer):
    class Meta:
        model = country
        fields = '__all__'

class Customer(serializers.ModelSerializer):
    class Meta:
        model = customer
        fields = '__all__'

    def _init_(self, *args, **kwargs):

        super(Customer, self)._init_(*args, **kwargs)

        self.fields["country_id"].queryset = Country.objects.all()    

class Product_Category(serializers.ModelSerializer):
    class Meta:
        model = product_category
        fields = '__all__'

class Product(serializers.ModelSerializer):
    class Meta:
        model = product
        fields = '__all__'

    def _init_(self, *args, **kwargs):

        super(Product, self)._init_(*args, **kwargs)

        self.fields["product_category_id"].queryset = Product_Category.objects.all()  

class Installation_Category(serializers.ModelSerializer):
    class Meta:
        model = installation
        fields = '__all__'

class Installation(serializers.ModelSerializer):
    class Meta:
        model = installation
        fields = '__all__'

    def _init_(self, *args, **kwargs):

        super(Product, self)._init_(*args, **kwargs)

        self.fields["product_id"].queryset = Product.objects.all()  

    def _init_(self, *args, **kwargs):

        super(Customer, self)._init_(*args, **kwargs)

        self.fields["customer_id"].queryset = Customer.objects.all()  
