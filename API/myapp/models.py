from django.db import models

# Create your models here.
class country(models.Model):
    name = models.CharField(max_length=100)
    region = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class customer(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    country_id = models.ForeignKey(country, on_delete=models.CASCADE)
    permium_customer = models.CharField(max_length=100)
    

    def __str__(self):
        return self.name

class product_category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name       

class product(models.Model):
    reference = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    product_category_id = models.ForeignKey(product_category, on_delete=models.CASCADE)
    price = models.CharField(max_length=100)

    def __str__(self):
        return self.name 

class installation(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    product_id = models.ForeignKey(product, on_delete=models.CASCADE)
    customer_id = models.ForeignKey(customer, on_delete=models.CASCADE)
    installation_date = models.DateField()

    def __str__(self):
        return self.name   