from django.shortcuts import render

# Create your views here.

from .models import *
from .serializers import *
from rest_framework.generics import (ListCreateAPIView, RetrieveUpdateDestroyAPIView)
from django.shortcuts import get_object_or_404

class country_list(ListCreateAPIView):
    serializer_class = Country
    
    def get_queryset(self):
        return country.objects.all()
    
    def perform_create(self,serializers):
        serializers.save()

class country_details(RetrieveUpdateDestroyAPIView):
    serializer_class = Country

    def get_object(self):
        return get_object_or_404 (country,pk=self.kwargs.get('id'))

    def get_serializers_context(self):
        context = super(info,self).get_serializer_context()
        return context
        
class customer_list(ListCreateAPIView):
    serializer_class = Customer
    
    def get_queryset(self):
        return customer.objects.all()
    
    def perform_create(self,serializers):
        serializers.save()

class customer_details(RetrieveUpdateDestroyAPIView):
    serializer_class = Customer

    def get_object(self):
        return get_object_or_404 (customer,pk=self.kwargs.get('id'))

    def get_serializers_context(self):
        context = super(info,self).get_serializer_context()
        return context

class product_category_list(ListCreateAPIView):
    serializer_class = Product_Category
    
    def get_queryset(self):
        return product_category.objects.all()
    
    def perform_create(self,serializers):
        serializers.save()

class product_category_details(RetrieveUpdateDestroyAPIView):
    serializer_class = Product_Category

    def get_object(self):
        return get_object_or_404 (product_category,pk=self.kwargs.get('id'))

    def get_serializers_context(self):
        context = super(info,self).get_serializer_context()
        return context

class product_list(ListCreateAPIView):
    serializer_class = Product
    
    def get_queryset(self):
        return product.objects.all()
    
    def perform_create(self,serializers):
        serializers.save()

class product_details(RetrieveUpdateDestroyAPIView):
    serializer_class = Product

    def get_object(self):
        return get_object_or_404 (product,pk=self.kwargs.get('id'))

    def get_serializers_context(self):
        context = super(info,self).get_serializer_context()
        return context

class installation_list(ListCreateAPIView):
    serializer_class = Installation
    
    def get_queryset(self):
        return installation.objects.all()
    
    def perform_create(self,serializers):
        serializers.save()

class installation_details(RetrieveUpdateDestroyAPIView):
    serializer_class = Installation

    def get_object(self):
        return get_object_or_404 (installation,pk=self.kwargs.get('id'))

    def get_serializers_context(self):
        context = super(info,self).get_serializer_context()
        return context

        

