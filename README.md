# 🦾BestPraticeIBA 

IBA Test

## 📈 Data Warehouse: 
1. File location: 
	[GitLab](https://gitlab.com/rvsolanki97/TestOfIBA/-/tree/main/API)
  The Comment to Run the API:
	> python manage.py runserver
  
  It will show you my API with your given Database.

2. Database Location:
	[GitLab](https://gitlab.com/rvsolanki97/TestOfIBA/-/blob/main/API/db.sqlite3)
  

## 👓 Data API

1. I tested your API:
	[GitLab](https://gitlab.com/rvsolanki97/TestOfIBA/-/tree/main/awesome-inc-main)
  
  ![image](https://user-images.githubusercontent.com/57105052/154164182-9e3b11c9-aea8-46d2-b018-bd403bedc457.png)

2. Helm Chart:
	[GitLab](https://gitlab.com/rvsolanki97/TestOfIBA/-/tree/main/HelmChart/chart.yaml)
  I have created sample a Helm Chart, but i know the command to create a Helm Chart by gitlab, this all are basice command to use in Helm Chart while Creating 

	> helm create --namespace

  > kubectle expose deploy myapp --port 80 --dry-run -o yaml > templates/service.yaml

  > helm upgrades myapp

  > helm rollback myapp 1 or 2 (as per your choices)

  > helm delete --purge myapp

3. CI/CD Piepline 

For this i need to create a .gitlab-ci.yml  file for, this i dont know that much but i try to solve 

[GitLab](https://gitlab.com/rvsolanki97/TestOfIBA/-/blob/main/.gitlab-ci.yml)

Output 

![image.png](./image.png)

